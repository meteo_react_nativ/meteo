import React, { useEffect, useState } from 'react';

const [list_weather_forcast, setList_weather_forcast] = useState(null);

const [jour1IconUrl, setJour1IconUrl] = useState(null);
const [jour2IconUrl, setJour2IconUrl] = useState(null);
const [jour3IconUrl, setJour3IconUrl] = useState(null);
const [jour4IconUrl, setJour4IconUrl] = useState(null);
const [jour5IconUrl, setJour5IconUrl] = useState(null);

const [idicon, setIdicon] = useState(null);
const [weather, setWeather ] = useState(null);
const [icon, setIcon] = useState(null);

const getWeather = () => {
    return fetch('https://api.openweathermap.org/data/2.5/weather?lat='+latitude+'&lon='+longitude+'&units=metric&appid=a599fb78c05594ced1b67065fc699de9')
      .then(response => response.json())
      .then(json => {
        console.log(json);
        //envoyer json a weather
        setWeather(json);
        console.log(json.weather[0].icon);
        setIdicon(json.weather[0].icon);
        // return json;
      })
      .catch(error => {
        console.error(error);
      });
  };
  
  const getIcon = async () => {
  
    try {
      const response = await fetch('https://openweathermap.org/img/wn/'+idicon+'@2x.png');
      if (!response.ok) {
        throw new Error('Image non trouvée');
      }
      const imgUrl = response.url;
      setIcon(imgUrl);
      console.log(imgUrl);
    } catch (error) {
      console.error(error);
    }
  
  };
  
  const getFore = async () => {
  
    const response = await fetch('https://api.openweathermap.org/data/2.5/forecast?lat=44.34&lon=10.99&units=metric&appid=a599fb78c05594ced1b67065fc699de9');
    const response_forcast = await response.json();
    console.log("forecast :", response_forcast.list[0].weather[0].icon); 
    setList_weather_forcast(response_forcast);
    console.log("response_forcast : "+response_forcast);
  
    const jsonToJour1Id = response_forcast.list[0].weather[0].icon;
    const jsonToJour2Id = response_forcast.list[1].weather[0].icon;
    const jsonToJour3Id = response_forcast.list[2].weather[0].icon;
    const jsonToJour4Id = response_forcast.list[3].weather[0].icon;
    const jsonToJour5Id = response_forcast.list[4].weather[0].icon;
  
    console.log("jsonToJour1 : "+jsonToJour1Id);
    console.log("jsonToJour2 : "+jsonToJour2Id);
    console.log("jsonToJour3 : "+jsonToJour3Id);
    console.log("jsonToJour4 : "+jsonToJour4Id);
    console.log("jsonToJour5 : "+jsonToJour5Id);
  
    const jour2icon = await fetch('https://openweathermap.org/img/wn/'+jsonToJour2Id+'@2x.png');
    const jour1icon = await fetch('https://openweathermap.org/img/wn/'+jsonToJour1Id+'@2x.png');
    const jour3icon = await fetch('https://openweathermap.org/img/wn/'+jsonToJour3Id+'@2x.png');
    const jour4icon = await fetch('https://openweathermap.org/img/wn/'+jsonToJour4Id+'@2x.png');
    const jour5icon = await fetch('https://openweathermap.org/img/wn/'+jsonToJour5Id+'@2x.png');
  
    console.log("jour1icon" + jour1icon);
    console.log("jour2icon" + jour2icon);
    console.log("jour3icon" + jour3icon);
    console.log("jour4icon" + jour4icon);
    console.log("jour5icon" + jour5icon);
  
    console.log("jour1icon.url : "+jour1icon.url + "type : "+typeof(jour1icon.url));
    console.log("jour2icon.url : "+jour2icon.url);
    console.log("jour3icon.url : "+jour3icon.url);
    console.log("jour4icon.url : "+jour4icon.url);
    console.log("jour5icon.url : "+jour5icon.url);
  
    const jour1icon_recup = jour1icon.url;
    const jour2icon_recup = jour2icon.url;
    const jour3icon_recup = jour3icon.url;
    const jour4icon_recup = jour4icon.url;
    const jour5icon_recup = jour5icon.url;
  
    console.log("jour1icon_recup :", jour1icon_recup + "type : "+typeof(jour1icon.url));
    console.log("jour2icon_recup :", jour2icon_recup);
    console.log("jour3icon_recup :", jour3icon_recup);
    console.log("jour4icon_recup :", jour4icon_recup);
    console.log("jour5icon_recup :", jour5icon_recup);
  
    setJour1IconUrl(jour1icon_recup);
    setJour2IconUrl(jour2icon_recup);
    setJour3IconUrl(jour3icon_recup);
    setJour4IconUrl(jour4icon_recup);
    setJour5IconUrl(jour5icon_recup);
  
    console.log(jour1IconUrl);
    console.log(jour2IconUrl);
    console.log(jour3IconUrl);
    console.log(jour4IconUrl);
    console.log(jour5IconUrl);
  }

  export {
    getWeather,
    getIcon,
    getFore,
    setJour1IconUrl,
    setJour2IconUrl,
    setJour3IconUrl,
    setJour4IconUrl,
    setJour5IconUrl,
    setIdicon,
    setWeather,
    setIcon,
    setList_weather_forcast,
  };